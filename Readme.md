****make dir****

1. mkdir image

2. cd model
   
   mkdir checkpoint

download model in https://drive.google.com/open?id=1Cpzb8evt4fHyj4Yfl8IN0sy0amKYu0lG

copy the model to the checkpoint directory

****environment****

pip install tensorflow==1.14.0

pip install opencv-python==3.4.5.20

pip install imutils

pip install pytesseract

pip install pillow

pip install cython

After installing the available libraries:

cd model/utils/bbox

chmod +x make.sh

./make.sh

****run****

activate environment and run

python main.py
