#import library
import numpy as np
import time
import os
import cv2
import tensorflow as tf
import glob
import matplotlib.pyplot as plt
from model.nets import model_train
import re
import imutils
#import model ctpn
from model.utils.rpn_msr.proposal_layer import proposal_layer
from model.utils.text_connector.detectors import TextDetector
#import model text
import model.text.vie.crop_char_vie as ccvie
import model.text.eng.crop_char_eng as cceng
import model.text.vie.text_vie as gtfivie
import model.text.eng.text_eng as gtfieng
import model.text.categorize as ctg

#restore checkpoint ctpn
with tf.get_default_graph().as_default():
    input_image = tf.placeholder(tf.float32, shape=[None, None, None, 3], name='input_image')
    input_im_info = tf.placeholder(tf.float32, shape=[None, 3], name='input_im_info')
    bbox_pred, cls_pred, cls_prob = model_train.model(input_image)
    global_step = tf.get_variable('global_step', [], initializer=tf.constant_initializer(0), trainable=False)
    variable_averages = tf.train.ExponentialMovingAverage(0.997, global_step)
    saver = tf.train.Saver(variable_averages.variables_to_restore())
    sess = tf.Session()
    saver.restore(sess, "model/checkpoint/ctpn_50000.ckpt")

#resize image
def resize_image(img):
    img_size = img.shape
    im_size_min = np.min(img_size[0:2])
    im_size_max = np.max(img_size[0:2])

    im_scale = float(600) / float(im_size_min)
    if np.round(im_scale * im_size_max) > 1200:
        im_scale = float(1200) / float(im_size_max)
    new_h = int(img_size[0] * im_scale)
    new_w = int(img_size[1] * im_scale)

    new_h = new_h if new_h // 16 == 0 else (new_h // 16 + 1) * 16
    new_w = new_w if new_w // 16 == 0 else (new_w // 16 + 1) * 16

    re_im = cv2.resize(img, (new_w, new_h), interpolation=cv2.INTER_LINEAR)
    return re_im, (new_h / img_size[0], new_w / img_size[1])

#get list image crop using model ctpn
def get_crop_using_ctpn(image, image_for_crop):
    list_image_crop = []
    image_for_crop, (rh, rw) = resize_image(image_for_crop)
    img, (rh, rw) = resize_image(image)
    h, w, c = img.shape
    im_info = np.array([h, w, c]).reshape([1, 3])
    bbox_pred_val, cls_prob_val = sess.run([bbox_pred, cls_prob],
                                            feed_dict={input_image: [img],
                                                        input_im_info: im_info})

    textsegs, _ = proposal_layer(cls_prob_val, bbox_pred_val, im_info)
    scores = textsegs[:, 0]
    textsegs = textsegs[:, 1:5]
    textdetector = TextDetector(DETECT_MODE='H')
    boxes = textdetector.detect(textsegs, scores[:, np.newaxis], img.shape[:2])
    boxes = np.array(boxes, dtype=np.int)
    my_sorted_list = sorted(boxes, key=lambda x: x[1])
    for i, box in enumerate(my_sorted_list):
        x_min = box[0]
        y_min = box[1]
        x_max = box[4]
        y_max = box[5]
        h = y_max - y_min
        w = x_max - x_min
        if w/h < 2:
            pass      
        else:
            if (x_min>5) and (x_max<(img.shape[1]-5)) and (y_min>5) and (y_max<(img.shape[0]-5)):
                if h > 30:
                    x_min_crop = int(box[0]-5)
                    x_max_crop = int(box[4]+5)
                    y_min_crop = int(box[1]-5)
                    y_max_crop = int(box[5]+5)
                else:
                    x_min_crop = int(box[0]-3)
                    x_max_crop = int(box[4]+3)
                    y_min_crop = int(box[1]-3)
                    y_max_crop = int(box[5]+3)
            else:
                x_min_crop = x_min
                x_max_crop = x_max
                y_min_crop = y_min
                y_max_crop = y_max
            crop = image_for_crop[y_min_crop:y_max_crop,x_min_crop:x_max_crop]
            list_image_crop.append(crop)
    return list_image_crop

#get list image crop using opencv
def get_crop_using_opencv(img):
    img = imutils.resize(img, height=600)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    sobelx = cv2.Sobel(gray,cv2.CV_8U, 1, 0, ksize=3)
    __, threshold = cv2.threshold(sobelx, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    morph = threshold.copy()
    cv2.morphologyEx(src=threshold, op=cv2.MORPH_CLOSE, kernel=cv2.getStructuringElement(shape=cv2.MORPH_RECT, ksize=(35, 1)), dst=morph)
    new,contours, hierarchy = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    S = img.shape[0] * img.shape[1]
    list_image_crop = []
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        s = w*h
        x_max = x+w
        y_max = y+h
        if w/h > 2 and s/S < 0.1 and s/S > 0.0001 and s > 1000:
            if (x>10) and (x_max<(img.shape[1]-10)) and (y>10) and (y_max<(img.shape[0]-10)):
                if h > 30:
                    x_min_crop = int(x-10)
                    x_max_crop = int(x_max+10)
                    y_min_crop = int(y-10)
                    y_max_crop = int(y_max+10)
                else:
                    x_min_crop = int(x-6)
                    x_max_crop = int(x_max+6)
                    y_min_crop = int(y-6)
                    y_max_crop = int(y_max+6)
            else:
                x_min_crop = x
                x_max_crop = x_max
                y_min_crop = y
                y_max_crop = y_max
            crop = img[y_min_crop:y_max_crop,x_min_crop:x_max_crop]
            list_image_crop.append(crop)
    return list_image_crop

#process get crop text in input image
def process_image_input(image, image_for_crop, method_process):
    if method_process == 'ctpn':
        list_image_for_get_text = get_crop_using_ctpn(image, image_for_crop)
    elif method_process == 'opcv':
        list_image_for_get_text = get_crop_using_opencv(image)
    return list_image_for_get_text

#process image crop with multi language
def process_image_crop(list_image_for_get_text, gtfi, cc):
    os.system("rm -rf image/*")
    text_out = []
    image_out = []
    image_list = []
    for crop in list_image_for_get_text:
        if crop.shape[0]>48:
            text = gtfi.get_text(crop)
            text_out.append(text)
            image_out.append(crop)
        elif 48>=crop.shape[0]>=30 and crop.shape[1] < 250:
            text = gtfi.get_text(crop)
            text_out.append(text)
            image_out.append(crop)
        elif 48>=crop.shape[0]>=30 and crop.shape[1] > 250 and crop.shape[1]/crop.shape[0] < 7:
            text = gtfi.get_text(crop)
            text_out.append(text)
            image_out.append(crop)
        elif 48>=crop.shape[0]>=30 and crop.shape[1] > 250 and crop.shape[1]/crop.shape[0] > 7:
            name, image_crop = cc.crop_col_ver2(crop)     
            for i in range(len(name)):
                text_out.append(name[i])
                image_out.append(image_crop[i])
        elif 30 >crop.shape[0]>=27 and crop.shape[1]>400 and crop.shape[1]/crop.shape[0] < 22:
            text = gtfi.get_text(crop)
            text_out.append(text)
            image_out.append(crop)
        elif 30 >crop.shape[0]>=27 and crop.shape[1]>400 and crop.shape[1]/crop.shape[0] > 22:
            name, image_crop = cc.crop_col_ver2(crop)     
            for i in range(len(name)):
                text_out.append(name[i])
                image_out.append(image_crop[i])
        else:
            name, image_crop = cc.crop_col_ver2(crop)      
            for i in range(len(name)):
                text_out.append(name[i])
                image_out.append(image_crop[i])
    for i in range(len(image_out)):
        name_image = 'image/'+str(time.time())+'.jpg'
        cv2.imwrite(name_image,image_out[i])
        image_list.append(name_image)
    return text_out, image_list

#get text with multi language
def get_text_in_crop(list_image_crop, language):
    if language == 'vie':
        text_out, image_list = process_image_crop(list_image_crop, gtfivie, ccvie)
    elif language == 'eng':
        text_out, image_list = process_image_crop(list_image_crop, gtfieng, cceng)
    return text_out, image_list

#grouping of texts
def class_text(text_out, image_list):
    dict_text, dict_image = ctg.class_card(text_out,image_list)
    return dict_text, dict_image

#main
def general(path, method_process, language):
    image = cv2.imread(path)
    image_for_crop = image.copy()
    image_list_crop = process_image_input(image, image_for_crop, method_process)
    text_list_out, image_list_out = get_text_in_crop(image_list_crop, language)
    dict_text_out, dict_image_out = class_text(text_list_out, image_list_out)
    return dict_text_out, dict_image_out