import pytesseract
import cv2
import time
from PIL import Image
import os
import glob
import model.text.vie.text_replace as tp

lang_vie = 'vie'
multilang = 'eng+vie'
config_eng = '--psm 7  -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyz'
multiconfig = '--psm 7 --oem 3 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyzÂÊÔàáâãèéêìíòóôõùúýăĐđĩũƠơưạảấầẩậắằẵặẻẽếềểễệỉịọỏốồổỗộớờởỡợụủỨứừửữựỳỵỷỹ'

def threshold_img(img):
    image_area =  img.shape[1] * img.shape[0]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    __, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    area = cv2.countNonZero(threshold) #cv2.countNonZero count white pixel 
    ratio = area/image_area
    if ratio < 0.5:
        __, out = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    else:
        out = threshold.copy()
    return out

list_eng_char = ['er', 'al', 'st', 'nk', 'city', 'ie', 'dist', 'net', 'jp', 
                'ward', 'ph.d', 'tion', 'mail', 'yahoo', 'co', 'email', 'tel', 'fax']

def get_text(img):
    img = threshold_img(img)
    ratio =  img.shape[1] / img.shape[0]
    if ratio <= 3:
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
        text_eng = pytesseract.image_to_string(img,lang='eng',config=config_eng)
        text_vie_lower = text_vie.lower()
        count = 0
        # print(text_vie_lower)
        for i in text_vie_lower:
            if i == '@' or i == 'w' or i =='/' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                count +=1 
            if i == 'ÿ':
                text_vie = ''
        numbers = sum(digit.isdigit()for digit in text_vie_lower)
        if numbers>3:
            count+=1
        for char in list_eng_char:
            temp = text_vie_lower.find(char)
            if temp != -1:
                count +=1
        if count == 0:
            text_out = text_vie
        else:
            text_out = text_eng
        clear = text_out.find('|')
        clear1 = text_out.find('KHSA')
        if clear != -1 or clear1 != -1:
            text_out = ''
        text_out = tp.replace_text(text_out)
        if len(text_out) < 3:
            text_out = ''
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # print(img.shape,'       ',text_out,'       ',ratio)
    elif 3 < ratio <5:
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
        text_eng = pytesseract.image_to_string(img,lang='eng',config=config_eng)
        count = 0
        text_vie_lower = text_vie.lower()
        # print(text_vie_lower)
        for i in text_vie_lower:
            if i == '@' or i == 'w' or i =='/' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                count +=1 
            if i == 'ÿ':
                text_vie = ''
        numbers = sum(digit.isdigit()for digit in text_vie_lower)
        if numbers>3:
            count+=1
        for char in list_eng_char:
            temp = text_vie_lower.find(char)
            if temp != -1:
                count +=1
        if count != 0 or img.shape[0] > 60:
            text_out = text_eng
        else:
            text_out = text_vie 
        if len(text_out) < 2:
            text_out = ''
        text_out = tp.replace_text(text_out)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # print(img.shape,'       ',text_out,'       ',ratio)
    elif 5 <= ratio <=6:
        text_eng = pytesseract.image_to_string(img,lang='eng',config=config_eng)
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multilang)
        if len(text_vie) != 0:
            count = 0
            text_vie_lower = text_vie.lower()
            
            for i in text_vie_lower:
                if i == '@' or i == 'w' or i =='/' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                    count +=1 
                if i == 'ÿ':
                    text_vie = ''
            numbers = sum(digit.isdigit()for digit in text_vie_lower)
            if numbers>3:
                count+=1
            for char in list_eng_char:
                temp = text_vie_lower.find(char)
                if temp != -1:
                    count +=1
            if count == 0:
                text_out = text_vie
            else:
                text_out = text_eng
            text_out = tp.replace_text(text_out)
        else:
            text_out = text_eng
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # print(img.shape,'       ',text_out,'       ',ratio)
    elif 6 < ratio <=7:
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
        text_eng = pytesseract.image_to_string(img,lang='eng',config=config_eng)
        count = 0
        text_vie_lower = text_vie.lower()
        # print(text_vie_lower)
        for i in text_vie_lower:
            if i == '@' or i == 'w' or i =='/' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                count +=1 
            if i == 'ÿ':
                text_vie = ''
        numbers = sum(digit.isdigit()for digit in text_vie_lower)
        if numbers>3:
            count+=1
        for char in list_eng_char:
            temp = text_vie_lower.find(char)
            if temp != -1:
                count +=1
        if count != 0:
            text_out = text_eng
        else:
            text_out = text_vie  
        if numbers > 3:
            for character in text_out:
                if character.isalnum():
                    pass
                else:
                    if character == ' ' or character == '+':
                        pass
                    else:
                        text_out = text_out.replace(character,'')
        text_out = tp.replace_text(text_out)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # print(img.shape,'       ',text_out,'       ',ratio)
    elif ratio > 9:
        text_multi = pytesseract.image_to_string(img,lang=multilang,config=multiconfig)
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
        count = 0
        text_vie_lower = text_vie.lower()
        count = 0
        # print(text_vie_lower)
        for i in text_vie_lower:
            if i == '@' or i == 'w' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                count +=1 
            if i == 'ÿ':
                text_vie = ''
        for char in list_eng_char:
            temp = text_vie_lower.find(char)
            if temp != -1:
                count +=1
        if count==0:
            text_out = text_vie
        else:
            text_out = text_multi
        result = text_out.split(' ')
        # print(result)
        text_out = tp.replace_text(text_out)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # print(img.shape,'       ',text_out,'       ',ratio)
    elif 7 < ratio <=8:
        # print(i)
        
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
        text_eng = pytesseract.image_to_string(img,lang='eng',config=config_eng)
        # text = text.replace('http-//','http://')
        count = 0
        text_vie_lower = text_vie.lower()
        count = 0
        # print(text_vie_lower)
        for i in text_vie_lower:
            if i == '@' or i == 'w' or i =='/' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                count +=1 
            if i == 'ÿ':
                text_vie = ''
        numbers = sum(digit.isdigit()for digit in text_vie_lower)
        if numbers>3:
            count+=1
        for char in list_eng_char:
            temp = text_vie_lower.find(char)
            if temp != -1:
                count +=1
        if count == 0:
            text_out = text_vie
        else:
            text_out = text_eng
        
        text_out = tp.replace_text(text_out)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        # print(img.shape,'       ',text_out,'       ',ratio)
    else:
        pass
        text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
        text_eng = pytesseract.image_to_string(img,lang='eng',config=config_eng)
        text_vie_lower = text_vie.lower()
        count = 0
        # print(text_vie_lower)
        for i in text_vie_lower:
            if i == '@' or i == 'w' or i =='/' or i == 'j' or i == 'z' or i == '+' or i == 'f' or i =='%':
                count +=1 
            if i == 'ÿ':
                text_vie = ''
        if count == 0:
            text_out = text_vie
        else:
            text_out = text_eng
        text_out = tp.replace_text(text_out)
        # cv2.imshow('img',img)
        # cv2.waitKey(0) 
        # print(img.shape,'       ',text_out,'       ',ratio)
    return text_out