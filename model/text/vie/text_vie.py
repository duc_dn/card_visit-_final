import pytesseract
import cv2
import time
from PIL import Image
import os
import glob
import re
import model.text.vie.text_replace as tp

lang_vie = 'vie'
lang_eng = 'eng'
multilang = 'eng+vie'
config = '--psm 7  -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyz'
multiconfig = '--psm 7 --oem 3 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyzÂÊÔàáâãèéêìíòóôõùúýăĐđĩũƠơưạảấầẩậắằẵặẻẽếềểễệỉịọỏốồổỗộớờởỡợụủỨứừửữựỳỵỷỹ'

def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

def threshold_img(img):
    image_area =  img.shape[1] * img.shape[0]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    __, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    area = cv2.countNonZero(threshold) #cv2.countNonZero count white pixel 
    ratio = area/image_area
    if ratio < 0.5:
        __, out = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    else:
        out = threshold.copy()
    return out

list_eng_char = ['er', 'al', 'st', 'nk', 'city', 'ie', 'dist', 
                'ward', 'ph.d', 'tion', 'mail', 'yahoo', 'co', 'email', 'tel', 'fax']

def get_text(img):
    img = threshold_img(img)
    text_eng = pytesseract.image_to_string(img,lang='eng',config=config)
    text_vie = pytesseract.image_to_string(img,lang='vie',config=multiconfig)
    # print(text_eng)
    # print(text_vie)
    text_vie_lower = text_vie.lower()
    count = 0
    for i in text_vie_lower:
        if i == '@' or i == 'w' or i == 'j' or i == '+' or i =='%':
            count +=1 
    for char in list_eng_char:
        temp = text_vie_lower.find(char)
        if temp != -1:
            count +=1
    if count == 0:
        text_out = text_vie
    else:
        text_out = text_eng
    text_out = tp.replace_text(text_out)
    # cv2.imshow('img',img)
    # cv2.waitKey(0)
    # print(img.shape,'       ',text_out+'\n')
                    # '                eng',text_eng+'\n',
                    # '                vie',text_vie+'\n',
                    # '                ',path+'\n')
    return text_out

