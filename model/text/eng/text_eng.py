import pytesseract
import cv2
import time
from PIL import Image
import model.text.eng.tp_eng as tp

def threshold_img(img):
    image_area =  img.shape[1] * img.shape[0]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    __, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    area = cv2.countNonZero(threshold) #cv2.countNonZero count white pixel 
    ratio = area/image_area
    if ratio < 0.5:
        __, out = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    else:
        out = threshold.copy()
    return out

config = '--psm 7  -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyz'

def get_text(img):
    img = threshold_img(img)
    text_eng = pytesseract.image_to_string(img,lang='eng',config=config)
    text_eng = tp.replace_text(text_eng)
    return text_eng