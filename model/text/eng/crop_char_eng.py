import cv2
import glob 
import os
import time
import numpy as np
import re
import model.text.eng.text_eng as gtfi

temp_list = np.array([0])
def clear_number_0(list_a,list_b,temp_list):
    try:
        list_a = np.setdiff1d(list_a,temp_list,True)
        list_b = np.setdiff1d(list_b,temp_list,True)
    except:
        pass 
    return list_a, list_b

def crop_col_ver2(img):
    text_ren = []
    text_out = ''
    image = []
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    __, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    if threshold[0][0] == 255:
        __, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)  
    img_col_sum = np.sum(threshold,axis=0).tolist()
    # _x = np.arange(threshold.shape[1])
    
    x_start = []
    x_end = []
    for i in range(len(img_col_sum)-1):
        if len(x_end) == 0 and len(x_start) == 0 and (img_col_sum[i] != 0) and (img_col_sum[i+1] == 0):
            pass
        elif (img_col_sum[i] == 0) and (img_col_sum[i+1] != 0):
            x_start.append(i)
        elif (img_col_sum[i] != 0) and (img_col_sum[i+1] == 0):
            x_end.append(i)    
    if len(x_start) > len(x_end):
        x_end.append(img.shape[1])
    for i in range(len(x_start)):
        if x_end[i]-x_start[i] < 3:
            x_end[i] = 0
            x_start[i] = 0
    
    x_start, x_end = clear_number_0(x_start, x_end, temp_list)
    
    if len(x_start) < len(x_end):
        x_start = np.insert(x_start, 0,0)
    for i in range(1,len(x_start)):
        sub = x_start[i]-x_end[i-1]
        if 10<=sub and sub!=11:
            pass
        else:
            x_start[i] = 0
            x_end[i-1] = 0
    x_start, x_end = clear_number_0(x_start, x_end, temp_list)  
    if len(x_start) < len(x_end):
        x_start = np.insert(x_start, 0,0)
    # print(x_start, x_end)
    if len(x_start)==0:
        w = img.shape[1]-15
        h = img.shape[0]
        if w/h <2:
            pass
        else:
            crop = img[0:img.shape[0],15:img.shape[1]]
            text = gtfi.get_text(crop)
            text_out += text
            text_ren.append(text_out)
    else:
        if len(x_start) == 1:
            text = gtfi.get_text(img)
            text_out += text
            text_ren.append(text_out)
        else:
            for i in range(len(x_start)):
                if x_start[i]>3 and int(x_end[i]+3)<img.shape[1]:
                    crop = img[0:img.shape[0],int(x_start[i]-3):int(x_end[i]+3)]
                else:
                    crop = img[0:img.shape[0],x_start[i]:x_end[i]]
                text = gtfi.get_text(crop)
                #int(x_start[i] - x_end[i-1])<20
                if i>=1:
                    if x_start[i]-x_end[i-1]>20:
                        text_ren.append(text_out)
                        text_out = ''
                        text_out += text
                        image.append(crop_copy)
                        crop_copy = crop.copy()
                    else:
                        text_out += ' '
                        text_out += text
                        crop_copy = np.concatenate((crop_copy, crop), axis=1)
                else:
                    text_out += text
                    crop_copy = crop.copy()
            text_out = text_out.replace(' co ie','.co.jp')
            text_ren.append(text_out)
            image.append(crop_copy)
    if len(image) == 0:
        image.append(img)
    else:
        pass
    return text_ren, image