import pytesseract
import re
import numpy as np
config = '--psm 7  -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYabcdeghiklmnopqrstuvxyz'
def get_text_no_thresh(img):
    text = pytesseract.image_to_string(img,lang='eng',config=config)
    return text

def return_web(a,b):
    if a!=-1 and b!=-1:
        return True
    else:
        return False

def check_text(phone, mail, web, add):
    new_phone = []
    for i in phone:
        if len(i) < 8:
            pass
        else:
            numbers = re.sub('[A-Za-z]','', i)
            numbers = re.sub('[^0-9+]',' ', numbers)
            new_phone.append(numbers)
    
    new_mail = []
    for i in mail:
        numbers = sum(digit.isdigit()for digit in i)
        if numbers/len(i) > 0.5:
            pass
        else:
            result = list(i.rpartition('@'))
            find_space = result[0].find(' ')
            find_colon = result[0].find(':')

            if find_colon != -1 and find_space == -1:
                name_mail = list(result[0].rpartition(':'))
                new = name_mail[-1]+'@'+result[-1]
            elif find_colon == -1 and find_space != -1:
                name_mail = list(result[0].rpartition(' '))
                new = name_mail[-1]+'@'+result[-1]
            elif find_colon == -1 and find_space == -1:
                name_mail = list(result[0].rpartition(' '))
                new = name_mail[-1]+'@'+result[-1]
            else:
                new = i
            new = new.replace('Email','')
            new = new.replace('email','')
            new = new.replace('EMAIL','')
            new = new.replace('Skype:','')
            
            new_mail.append(new)

    new_web = []
    for i in web:
        numbers = sum(digit.isdigit()for digit in i)
        if numbers/len(i) > 0.5:
            pass
        else:
            result = list(i.partition(' '))
            find_space = i.find(' ')
            if find_space == -1:
                new = result[0]
            else:
                new = result[-1]
            new = new.replace('Url','')
            new = new.replace('url','')
            new = new.replace('URL','')
            new = new.replace('Website','')
            new = new.replace('WEB','')
            new = new.replace('Web','')
            new = new.replace('web','')
            new = new.replace('Fanpage','')
            new = new.replace('website','')
            new = new.replace('fanpage','')
            new_web.append(new)

    new_add = []
    for i in add:
        result = list(i.partition(':'))
        find_colon = i.find(':')
        if find_colon != -1:
            new = result[-1]
        else:
            new = result[0]
        new = new.replace('add','')
        new = new.replace('Add','')
        new_add.append(new)
    return new_mail, new_web, new_phone, new_add

temp_list = np.array([0])
def clear_list_text(a):
    temp = a.copy()
    for i in range(len(temp)-1):
        for j in range(i+1,len(temp)):
            if temp[i] == temp[j]:
                a[j] = 0
    new_list = np.setdiff1d(a,temp_list,True)
    return new_list

work_name = ['chủ tịch', 'cae', 'caio', 'trợ lí', 'cdo', 'giám đốc', 'cgo', 'chief', 'deputy', 'consultant', 'engineer',
            'sáng lập', 'hội đồng', 'employee', 'executive', 'thực tập sinh', 'chro', 'cán bộ', 'corporate officer', 'professional',
            'ciso', 'share', 'trưởng nhóm', 'cổ đông', 'cio', 'secretaryclerk', 'cino', 'cbo', 'it management', 'marketing',
            'trainee', 'chuyên viên', 'cco', 'crdo', 'nhân viên', 'cro', 'collaborator', 'senior', 'transformation', 'office', 
            'cito', 'cto', 'cxo', 'leader', 'cmo', 'cvo', 'ceo', 'quản lý', 'manager', 'cno', 'co-founder', 'manage',
            'cfo', 'cpo', 'expert', 'chairman', 'supervisor', 'cao', 'clo', 'cse', 'thành viên', 'dev', 'development', 
            'trưởng bộ phận', 'coo', 'founder', 'cko', 'cso', 'cbdo', 'thư ký', 'cộng tác viên', 'research',
            'president', 'officer', 'người giám sát', 'director', 'assistant', 'cengo', 'team', 'lead',
            'trưởng phòng', 'receptionist', 'cqo', 'quản trị', 'cwo', 'giáo viên', 'giảng viên', 'teacher']
family_names = ['nguyễn ','trần ','lê ','phạm ','hoàng ','huỳnh ','phan ', 'luong', 'lương', 'lâm', 'lam', 'tony', 'ha', ' ngo',
                'vũ ','võ ','đặng ','bùi ','đỗ ','hồ ','ngô ','dương ','lý ', 'trương ', 'nguyên', 'luu', 'lưu', 'liêu', 'lieu',
                'nguyen ','tran ','le ','pham ','hoang ','huynh ', 'nguyên ', 'mai', 'yasuda', 'nakanishi', 'dau',
                'vu ','vo ','dang ','bui ','do ','ho ','ngo ','duong ','ly ', 'truong ', 'yamashita', 'saito',
                'andou', 'ootsuka', 'wakano', 'kobayashi', 'takeda', 'fujii', 'gotou', 'kikuchi', 'sugahara', 
                'hayashi', 'oda', 'sasaki', 'suzuki', 'yamada', 'miura', 'masuda', 'okiyama', 'murakami', 'abe', 
                'saitou', 'yokoyama', 'iwai', 'mori', 'iwasaki', 'kimura', 'ono', 'ueno', 'nomura', 'yamazaki', 
                'wata', 'nakamura', 'hasegawa', 'ueda', 'kaneko', 'sugiyama', 'takahashi', 'chiba', 'maruta', 
                'iijima', 'masahiko', 'sano', 'koyama', 'miyamoto', 'kondou', 'matsuda', 'ishikawa', 'fujita', 'kitano',
                'oono', 'ishida', 'matsuo', 'sakurai', 'fujimoto', 'uchida', 'shibata', 'taniguchi', 'fujiwara', 
                'yoshida', 'liu', 'kubo', 'tanada', 'yousuke', 'morioka', 'hara', 'ishii', 'hashimoto', 'miyazaki', 
                'yamamoto', 'simizu', 'ikeda', 'aoki', 'okamoto', 'kojima', 'kishita', 'nishimura', 'takada', 'morita', 
                'endou', 'yamayama', 'oota', 'nakajima', 'sakai', 'takagi', 'itou', 'ogawa', 'maeda', 'tamura', 'matsumoto', 
                'imai', 'kudou', 'maruyama', 'arai', 'ogame', 'nakawara', 'hirano', 'yamaguchi', 'takeuchi', 'nakano', 
                'inoue', 'okada', 'matsui', 'watanabe', 'harada', 'kinoshita', 'fukuda', 'sugimoto', 'noguchi', 'satou', 'Yusuke',
                'katou', 'kouno', 'sakamoto']
list_add = ['add','floor','str.', 'city', 'dist', 'ward', ' HN', 'HCM', 'Kadoma', 'str', 'center',
            'build', 'bld', 'street', 'road', 'phường', 'đường', 'tower', 'hanoi', 'vietnam',
            'thành', 'phố', 'tỉnh', 'quận', 'tầng', 'nhà', 'số', 'tokyo', 'japan', 'sotokanda',
            'Hà Nội', 'Ha Noi', 'japan', 'đại cổ việt', 'đông anh']
end_http = ['.vn', '.jp', '.co', '.net', '. co', '.sg', '.asia']
list_company = ['đại', 'khoa', 'school', 'university', 'công', 'tnhh', 'group', 'corp',
                'viện', 'bộ', 'inc', 'co.', 'ltd', 'đại học', 'phòng', 'jsc', 'tổ chức']
phone_list = ['phone', 'fax', 'mobile']
delete_char = ['home', 'center', 'happy', 'shopping']
                
def class_card(input_list, image_list): 
    name, name_image = [], []
    work, work_image = [], []
    company, company_image = [], []
    mail, mail_image = [], []
    web, web_image = [], []
    phone, phone_image = [], []
    add, add_image = [], []
    temp_list = input_list.copy()
    list_delete = []
    # print(temp_list)
    def Diff(li1, li2): 
        li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2] 
        return li_dif

    for text in input_list:
        text_vie_lower = text.lower()
        #### mail and web
        for end in end_http:
            temp_end = text_vie_lower.find(end)
            if temp_end != -1:
                ind = temp_list.index(text)
                new_text = get_text_no_thresh(image_list[ind])
                new_text_lower = new_text.lower()
                if new_text_lower[0] == '@':
                    new_text_lower = new_text_lower.replace(new_text_lower[0],'')
                if text_vie_lower[0] == '@':
                    text_vie_lower = text_vie_lower.replace(text_vie_lower[0],'')
                find_mail_new = new_text_lower.find('@')
                find_mail = text_vie_lower.find('@')
                if find_mail != -1 or find_mail_new != -1:
                    list_delete.append(text)
                    mail_image.append(image_list[ind])
                    if find_mail_new == -1:
                        mail.append(text)
                    else:
                        mail.append(new_text)
                    result = list(text.rpartition('@'))
                    name_company = list(result[2].split('.'))[0]
                ####web
                if find_mail == -1 or find_mail_new == -1:
                    find_dot = text_vie_lower.find('.')
                    find_web = text_vie_lower.find('/')
                    find_dot_new = new_text_lower.find('.')
                    find_web_new = new_text_lower.find('/')
                    a = return_web(find_dot,find_web)
                    b = return_web(find_dot_new,find_web_new)
                    if text in list_delete:
                        pass
                    else:
                        list_delete.append(text)
                        if a == True and b == True:
                            web.append(new_text)
                            ind = temp_list.index(text)
                            web_image.append(image_list[ind])
                        elif a == True and b == False:
                            web.append(text)
                            ind = temp_list.index(text)
                            web_image.append(image_list[ind])
                        elif a == False and b == True:
                            web.append(new_text)
                            ind = temp_list.index(text)
                            web_image.append(image_list[ind])
                        else:
                            if len(text)>len(new_text):
                                web.append(text)
                            elif len(text)==len(new_text):
                                web.append(text)
                            else:
                                web.append(new_text)
                            ind = temp_list.index(text)
                            web_image.append(image_list[ind])
        ####phone number
        numbers = sum(digit.isdigit()for digit in text_vie_lower)
        count_alpha = sum(a.isalpha() for a in text_vie_lower)
        if numbers>5 and count_alpha<=8:
            # new_text_phone = re.sub(r'[\W]'," ",text)
            # new_text_phone = new_text_phone.replace(' 84 ','+84 ')
            phone.append(text)
            list_delete.append(text)
            ind = temp_list.index(text)
            phone_image.append(image_list[ind])
        for i in phone_list:
            if text_vie_lower.find(i) != -1:
                phone.append(text)
                list_delete.append(text)
                ind = temp_list.index(text)
                phone_image.append(image_list[ind])
    ####address
    clear_list = Diff(temp_list, list_delete)
    for text in clear_list:
        text_vie_lower = text.lower()
        for location in list_add:
            location = location.lower()
            temp_add = text_vie_lower.find(location) 
            if temp_add != -1:
                list_0 = []
                for i in list_company:
                    i = i.lower()
                    temp_cp = text_vie_lower.find(i)
                    list_0.append(temp_cp)
                count = list_0.count(-1)
                if count < len(list_0):
                    pass
                else:
                    add.append(text)
                    list_delete.append(text)
                    ind = temp_list.index(text)
                    add_image.append(image_list[ind])
                num = re.sub(r"[\D]",'',text)
                if len(num)>0:
                    add.append(text)
                    list_delete.append(text)
                    ind = temp_list.index(text)
                    add_image.append(image_list[ind])
    clear_list = Diff(temp_list, list_delete)
    # print(web)
    ####company
    for text in clear_list:
        text_lower = text.lower()
        text_nospace = text_lower.replace(' ','')
        try:
            if text_nospace.find(name_company) != -1:
                company.append(text)
            else:
                pass
        except:
            pass
        for i in list_company:
            # print(text_lower)
            find_company = text_lower.find(i)
            if find_company != -1:
                company.append(text)
    temp_cpn = company.copy()
    for i in temp_cpn:
        text_lower = i.lower()
        text_nospace = text_lower.replace(' ','')
        find_cpn = text_lower.find('ceo')
        if find_cpn != -1:
            company.remove(i)
        list_1 = []
        for wo in work_name:
            wo = wo.replace(' ','')
            temp_ = text_nospace.find(wo)
            list_1.append(temp_)
        count = list_1.count(-1)
        if count < len(list_1):
            company.remove(i)
        else:
            pass

    for i in company:
        list_delete.append(i)
        ind = temp_list.index(i)
        company_image.append(image_list[ind])
    clear_list = Diff(temp_list, list_delete)
    ####work
    # print(clear_list)
    temp_work = []
    # print(clear_list)
    try:
        for i in clear_list:
            i_low = i.lower()
            text_nonspace = i_low.replace(' ','')
            # print(text_nonspace)
            for wo in work_name:
                wo = wo.lower()
                wo = wo.replace(' ','')
                if text_nonspace.find(wo) != -1:

                    if len(wo)<5 and len(text_nonspace)<5:
                        temp_work.append(i)
                    elif len(wo)>=5 and len(text_nonspace)>=5:
                        temp_work.append(i)
                    else:
                        pass
        # print(temp_work)
        if len(temp_work)!=0:
            work.append(temp_work[0])
            ind_work = temp_list.index(temp_work[0])
            work_image.append(image_list[ind_work])
            list_delete.append(temp_work[0])
        else:
            work.append(clear_list[1])
            ind_work = temp_list.index(clear_list[1])
            work_image.append(image_list[ind_work])
            list_delete.append(clear_list[1])
    except:
        pass
    ###name
    clear_list = Diff(temp_list, list_delete)
    temp = clear_list.copy()
    try:
        for i in temp:
            i_nospace = i.replace(' ','')
            if len(i_nospace) < 7:
                clear_list.remove(i)
            find = i.find('|')
            if find != -1:
                try:
                    clear_list.remove(i)
                except:
                    pass
            for dele in delete_char:
                i_lower = i.lower()
                find_del = i_lower.find(dele)
                if find_del != -1:
                    try:
                        clear_list.remove(i)
                    except:
                        pass
        temp_name = []
        for text in clear_list:
            text_lower = text.lower()
            for i in family_names:
                i = i.lower()
                if text_lower.find(i) != -1:
                    temp_name.append(text)
                    # print(i)
        # print(clear_list)
        print('----------')
        print(temp_name)
        temp_name = clear_list_text(temp_name)
        
        if len(temp_name) != 0:
            name.append(temp_name[0])
            ind_name = temp_list.index(temp_name[0])
            name_image.append(image_list[ind_name])

        else:
            name.append(clear_list[0])
            ind_name = temp_list.index(clear_list[0])
            name_image.append(image_list[ind_name])
    except:
        pass
    try:
        mail, web, phone, add = check_text(phone, mail, web, add)
    except:
        mail = mail
        web = web
        phone = phone
        add = add
    name = dict.fromkeys(name)
    company = dict.fromkeys(company)
    work = dict.fromkeys(work)
    mail = dict.fromkeys(mail)
    web = dict.fromkeys(web)
    phone = dict.fromkeys(phone)
    add = dict.fromkeys(add)

    name_image = dict.fromkeys(name_image)
    company_image = dict.fromkeys(company_image)
    work_image = dict.fromkeys(work_image)
    mail_image = dict.fromkeys(mail_image)
    web_image = dict.fromkeys(web_image)
    phone_image = dict.fromkeys(phone_image)
    add_image = dict.fromkeys(add_image)

    dic_text = {}
    dic_image = {}
    dic_text['name'] = list(name)
    dic_text['company'] = list(company)
    dic_text['work'] = list(work)
    dic_text['mail'] = list(mail)
    dic_text['web'] = list(web)
    dic_text['phone'] = list(phone)
    dic_text['add'] = list(add)
    dic_image['name'] = list(name_image)
    dic_image['company'] = list(company_image)
    dic_image['work'] = list(work_image)
    dic_image['mail'] = list(mail_image)
    dic_image['web'] = list(web_image)
    dic_image['phone'] = list(phone_image)
    dic_image['add'] = list(add_image)
    return dic_text, dic_image